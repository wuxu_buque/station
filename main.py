import pandas as pd
from cleansing import GccData
from cleansing import HandData
from decrypt import Decrypt
from model import ChinaIncome

if __name__ == "__main__":
    # 读取GCC数据
    gcc = GccData.ReadData('./data/GCC0205.xlsx', 1)
    print(len(gcc))
    # gcc.to_excel('./data/out.xlsx', sheet_name='汇总', index=False)

    # 读取人工数据
    hand = HandData.ReadData('./data/1~12月基地云手工账单拆分明细0109.xlsx', 4)
    print(len(hand))

    # 归并
    df = pd.concat([hand, gcc], ignore_index=True, join='outer', axis=0)
    print(len(df))

    # 删除空行
    df.dropna(axis=0, how='all', inplace=True)

    df.to_excel('./data/out.xlsx', sheet_name='汇总', index=False)
    # print(df)

    # 读取已分析的数据
    df = pd.read_excel('./data/out.xlsx', sheet_name=0)

    # 1. 中国区
    (total, res) = Decrypt.ChinaDetail(df)
    print(res)
    Decrypt.ChinaDetailUpdate(total, res)

    # 2. 区域代表处
    res = Decrypt.RegionDetail(df)
    print(res)
    Decrypt.RegionDetailUpdate(res)

    # 3. 基地
    res = Decrypt.StationDetail(df)
    print(res)
    res = Decrypt.StationDetailUpdate(res)
    

    # # 2. 区域基地云收入排行
    # Decrypt.RegionRanking(df)

    # # 3. 基地云收入排行
    # Decrypt.BaseRanking(df)

    # # 4. 基地收入分析
    # #     1) 基地总收入
    # #     2) 基地各服务收入占比排行
    # #     3) 基地计算占比
    # #     4) 基地计算收入折扣
    # Decrypt.BaseDetail(df)

    # 5. 区域的基地云收入分析
    # for region in [
    #     '山东', 
    #     '四川', 
    #     '江苏', 
    #     '湖北', 
    #     '河北', 
    #     '黑龙江', 
    #     '重庆', 
    #     '湖南', 
    #     '江西', 
    #     '陕西', 
    #     '贵州', 
    #     '新疆', 
    #     '内蒙古', 
    #     '山西', 
    #     '吉林', 
    #     '北京']:
    #     Decrypt.RegionDetail(region, df)
