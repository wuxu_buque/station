# -*-coding:utf-8-*-
"""
Author : buque
date   : 
role   : 数据库连接
"""

import pymysql
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
import os

pymysql.install_as_MySQLdb()

class DBContext(object):
    _engine = None
    # 数据库连接配置
    _DB = {
        'user': 'root',
        'passwd': 'mysqldb',
        'host': '192.168.0.103',
        # 'host': '100.94.165.216',
        'port': 3306,
        'db': 'landscape',
    }

    def __init__(self, rw='r', db_key=None, need_commit=False, **settings):
        self.__db_key = db_key
        if not self.__db_key:
            if rw == 'w':
                self.__db_key = 'default'
            elif rw == 'r':
                self.__db_key = 'readonly'
        self.__engine = self.get_db_engine()
        self.need_commit = need_commit

    @staticmethod
    def init_db_config():
        # 加载环境变量
        env_path = os.path.abspath('./src/web.env')
        in_docker = os.getenv("DOCKER")
        if not in_docker:
          load_dotenv(dotenv_path=env_path, verbose=True, override=True)

        DBContext._DB['user'] = os.getenv('DB_USER')
        DBContext._DB['passwd'] = os.getenv('DB_PASSWD')
        DBContext._DB['host'] = os.getenv('DB_ADDRESS')
        DBContext._DB['port'] = os.getenv('DB_PORT')
        DBContext._DB['db'] = os.getenv('DB_NAME')

    @staticmethod
    def get_db_engine():
        if DBContext._engine:
            return DBContext._engine

        # DBContext.init_db_config()
        """初始化数据库连接"""
        db_url = ("mysql+mysqldb://{user}:{passwd}@{host}:{port}/{db}"
                  "?charset=utf8".format(**DBContext._DB))
        # DBContext._engine = create_engine(db_url,
        #                         pool_size=10,
        #                         max_overflow=-1,
        #                         pool_recycle=1000,
        #                         # encoding='utf8',
        #                         # convert_unicode=True,
        #                         echo=False)
        DBContext._engine = create_engine(db_url,
                                # pool_size=10,
                                # max_overflow=-1,
                                # pool_recycle=1000,
                                pool_pre_ping=True,
                                echo=False)
        return DBContext._engine

    @staticmethod
    def create_new_database():
        with DBContext.connect("mysql") as con:
            try:
                create_sql = " CREATE DATABASE IF NOT EXISTS %s CHARACTER SET utf8 COLLATE utf8_general_ci " % DBContext._DB['db']
                print(create_sql)
                con.execute(create_sql)
            except Exception as err:
                print("error CREATE DATABASE :", err)

    @staticmethod
    def connect(db=None):
        if not db:
            db = DBContext._DB['db']
        try:
            datebase = pymysql.connect(host=DBContext._DB['host'],
                                        user=DBContext._DB['user'],
                                        password=DBContext._DB['passwd'],
                                        database=db,
                                        charset="utf8")
            datebase.autocommit(True)
            return datebase.cursor()
        except Exception as err:
            print("connect error :", err)

    @staticmethod
    def prepare():
        # DBContext.init_db_config()
        # 检查，如果执行 select 1 失败，说明数据库不存在，然后创建一个新的数据库。
        try:
            with DBContext.connect() as con:
                con.execute("select 1")
                print("########### db exists ###########")
        except Exception as err:
            print("check MYSQL_DB error and create new one :", err)
            # 检查数据库失败，
            DBContext.create_new_database()
            # 执行数据初始化。

    @property
    def session(self):
        return self.__session

    def __enter__(self):
        self.__session = sessionmaker(bind=self.__engine)()
        return self.__session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.need_commit:
            if exc_type:
                self.__session.rollback()
            else:
                self.__session.commit()
        self.__session.close()

    def get_session(self):
        return self.__session

DBContext.prepare()
DBContext.get_db_engine()