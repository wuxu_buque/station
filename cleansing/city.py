city_china = {
    '阿坝藏族羌族自治州': '四川',
    '阿克苏': '新疆',
    '阿拉善盟': '内蒙古',
    '和林格尔': '内蒙古',
    '阿勒泰': '新疆',
    '阿里': '西藏',
    '安康': '陕西',
    '安庆': '安徽',
    '庐阳': '安徽',
    '安顺': '贵州',
    '凯里': '贵州',
    '安阳': '河南',
    '鞍山': '辽宁',
    '巴彦淖尔': '内蒙古',
    '巴音郭楞蒙古自治州': '新疆',
    '巴中': '四川',
    '白城': '吉林',
    '白山': '吉林',
    '白银': '甘肃',
    '百色': '广西',
    '蚌埠': '安徽',
    '包头': '内蒙古',
    '宝鸡': '陕西',
    '保定': '河北',
    '保山': '云南',
    '北海': '广西',
    '本溪': '辽宁',
    '毕节': '贵州',
    '滨州': '山东',
    '博尔塔拉蒙古自治州': '新疆',
    '沧州': '河北',
    '昌都': '西藏',
    '昌吉回族自治州': '新疆',
    '长春': '吉林',
    '长沙': '湖南',
    '马栏山': '湖南',
    '长治': '山西',
    '常德': '湖南',
    '常州': '江苏',
    '宜兴': '江苏',
    '巢湖': '安徽',
    '朝阳': '辽宁',
    '潮州': '广东',
    '郴州': '湖南',
    '成都': '四川',
    '承德': '河北',
    '池州': '安徽',
    '赤峰': '内蒙古',
    '崇左': '广西',
    '滁州': '安徽',
    '楚雄彝族自治州': '云南',
    '达州': '四川',
    '大理白族自治州': '云南',
    '大连': '辽宁',
    '大庆': '黑龙江',
    '大同': '山西',
    '大兴安岭': '黑龙江',
    '丹东': '辽宁',
    '德宏傣族景颇族自治州': '云南',
    '德阳': '四川',
    '德州': '山东',
    '迪庆藏族自治州': '云南',
    '定西': '甘肃',
    '东莞': '广东',
    '东营': '山东',
    '鄂尔多斯': '内蒙古',
    '鄂州': '湖北',
    '恩施土家族苗族自治州': '湖北',
    '防城港': '广西',
    '佛山': '广东',
    '福州': '福建',
    '抚顺': '辽宁',
    '抚州': '江西',
    '阜新': '辽宁',
    '阜阳': '安徽',
    '甘南州': '甘肃',
    '甘孜藏族自治州': '四川',
    '赣州': '江西',
    '赣江新区': '江西',
    '固原': '宁夏回族',
    '广安': '四川',
    '广元': '四川',
    '广州': '广东',
    '白云': '广东',
    '贵港': '广西',
    '贵阳': '贵州',
    '桂林': '广西',
    '果洛藏族自治州': '青海',
    '哈尔滨': '黑龙江',
    '哈密': '新疆',
    '海北藏族自治州': '青海',
    '海东': '青海',
    '海口': '海南',
    '澄迈': '海南',
    '海南藏族自治州': '青海',
    '海西蒙古族藏族自治州': '青海',
    '邯郸': '河北',
    '汉中': '陕西',
    '杭州': '浙江',
    '毫州': '安徽',
    '合肥': '安徽',
    '和田': '新疆',
    '河池': '广西',
    '河源': '广东',
    '菏泽': '山东',
    '贺州': '广西',
    '鹤壁': '河南',
    '鹤岗': '黑龙江',
    '黑河': '黑龙江',
    '衡水': '河北',
    '衡阳': '湖南',
    '红河': '云南',
    '呼和浩特': '内蒙古',
    '呼伦贝尔': '内蒙古',
    '湖州': '浙江',
    '葫芦岛': '辽宁',
    '怀化': '湖南',
    '淮安': '江苏',
    '靖江': '江苏',
    '淮北': '安徽',
    '淮南': '安徽',
    '黄冈': '湖北',
    '黄南藏族自治州': '青海',
    '黄山': '安徽',
    '黄石': '湖北',
    '惠州': '广东',
    '鸡西': '黑龙江',
    '吉安': '江西',
    '吉林': '吉林',
    '济南': '山东',
    '济宁': '山东',
    '龙口': '山东',
    '佳木斯': '黑龙江',
    '嘉兴': '浙江',
    '嘉峪关': '甘肃',
    '江门': '广东',
    '焦作': '河南',
    '揭阳': '广东',
    '金昌': '甘肃',
    '金华': '浙江',
    '锦州': '辽宁',
    '晋城': '山西',
    '晋中': '山西',
    '荆门': '湖北',
    '荆州': '湖北',
    '景德镇': '江西',
    '九江': '江西',
    '酒泉': '甘肃',
    '喀什': '新疆',
    '开封': '河南',
    '克拉玛依': '新疆',
    '克孜勒苏柯尔克孜自治州': '新疆',
    '昆明': '云南',
    '拉萨': '西藏',
    '来宾': '广西',
    '莱芜': '山东',
    '兰州': '甘肃',
    '廊坊': '河北',
    '乐山': '四川',
    '丽江': '云南',
    '丽水': '浙江',
    '连云港': '江苏',
    '凉山彝族自治州': '四川',
    '辽阳': '辽宁',
    '辽源': '吉林',
    '聊城': '山东',
    '林芝': '西藏',
    '临沧': '云南',
    '临汾': '山西',
    '临夏州': '甘肃',
    '临沂': '山东',
    '柳州': '广西',
    '六安': '安徽',
    '六盘水': '贵州',
    '龙岩': '福建',
    '陇南': '甘肃',
    '娄底': '湖南',
    '泸州': '四川',
    '吕梁': '山西',
    '洛阳': '河南',
    '漯河': '河南',
    '马鞍山': '安徽',
    '茂名': '广东',
    '眉山': '四川',
    '梅州': '广东',
    '绵阳': '四川',
    '牡丹江': '黑龙江',
    '内江': '四川',
    '那曲': '西藏',
    '南昌': '江西',
    '南充': '四川',
    '南京': '江苏',
    '南宁': '广西',
    '南平': '福建',
    '南通': '江苏',
    '南阳': '河南',
    '宁波': '浙江',
    '宁德': '福建',
    '怒江傈僳族自治州': '云南',
    '攀枝花': '四川',
    '盘锦': '辽宁',
    '平顶山': '河南',
    '平凉': '甘肃',
    '萍乡': '江西',
    '莆田': '福建',
    '濮阳': '河南',
    '普洱': '云南',
    '七台河': '黑龙江',
    '齐齐哈尔': '黑龙江',
    '黔东南': '贵州',
    '黔南': '贵州',
    '黔西南': '贵州',
    '钦州': '广西',
    '秦皇岛': '河北',
    '青岛': '山东',
    '即墨': '山东',
    '清远': '广东',
    '庆阳': '甘肃',
    '曲靖': '云南',
    '衢州': '浙江',
    '泉州': '福建',
    '日喀则': '西藏',
    '日照': '山东',
    '肥城': '山东',
    '三门峡': '河南',
    '三明': '福建',
    '三亚': '海南',
    '山南': '西藏',
    '汕头': '广东',
    '汕尾': '广东',
    '商洛': '陕西',
    '商丘': '河南',
    '上饶': '江西',
    '韶关': '广东',
    '邵阳': '湖南',
    '绍兴': '浙江',
    '深圳': '广东',
    '沈阳': '辽宁',
    '十堰': '湖北',
    '石家庄': '河北',
    '曹妃甸': '河北',
    '石嘴山': '宁夏',
    '双鸭山': '黑龙江',
    '朔州': '山西',
    '四平': '吉林',
    '松原': '吉林',
    '苏州': '江苏',
    '宿迁': '江苏',
    '宿州': '安徽',
    '绥化': '黑龙江',
    '随州': '湖北',
    '遂宁': '四川',
    '塔城': '新疆',
    '台州': '浙江',
    '太原': '山西',
    '泰安': '山东',
    '泰州': '江苏',
    '靖江': '江苏',
    '唐山': '河北',
    '天水': '甘肃',
    '铁岭': '辽宁',
    '通化': '吉林',
    '通辽': '内蒙古',
    '铜川': '陕西',
    '铜陵': '安徽',
    '铜仁': '贵州',
    '吐鲁番': '新疆',
    '威海': '山东',
    '潍坊': '山东',
    '渭南': '陕西',
    '温州': '浙江',
    '文山壮族苗族自治州': '云南',
    '乌海': '内蒙古',
    '乌兰察布': '内蒙古',
    '乌鲁木齐': '新疆',
    '无锡': '江苏',
    '吴忠': '宁夏回族',
    '芜湖': '安徽',
    '梧州': '广西',
    '武汉': '湖北',
    '武威': '甘肃',
    '西安': '陕西',
    '西宁': '青海',
    '西双版纳傣族自治州': '云南',
    '锡林郭勒盟': '内蒙古',
    '厦门': '福建',
    '咸宁': '湖北',
    '咸阳': '陕西',
    '湘潭': '湖南',
    '湘西土家族苗族自治州': '湖南',
    '襄阳': '湖北',
    '孝感': '湖北',
    '忻州': '山西',
    '新乡': '河南',
    '新余': '江西',
    '信阳': '河南',
    '兴安盟': '内蒙古',
    '邢台': '河北',
    '徐州': '江苏',
    '许昌': '河南',
    '宣城': '安徽',
    '雅安': '四川',
    '烟台': '山东',
    '延安': '陕西',
    '延边朝鲜族自治州': '吉林',
    '盐城': '江苏',
    '扬州': '江苏',
    '阳江': '广东',
    '阳泉': '山西',
    '伊春': '黑龙江',
    '伊犁哈萨克自治州': '新疆',
    '宜宾': '四川',
    '宜昌': '湖北',
    '宜春': '江西',
    '益阳': '湖南',
    '银川': '宁夏回族',
    '鹰潭': '江西',
    '营口': '辽宁',
    '永州': '湖南',
    '榆林': '陕西',
    '玉林': '广西',
    '玉树藏族自治州': '青海',
    '玉溪': '云南',
    '岳阳': '湖南',
    '云浮': '广东',
    '运城': '山西',
    '枣庄': '山东',
    '湛江': '广东',
    '张家界': '湖南',
    '张家口': '河北',
    '张掖': '甘肃',
    '漳州': '福建',
    '昭通': '云南',
    '肇庆': '广东',
    '镇江': '江苏',
    '郑州': '河南',
    '中山': '广东',
    '中卫': '宁夏',
    '舟山': '浙江',
    '周口': '河南',
    '株洲': '湖南',
    '珠海': '广东',
    '驻马店': '河南',
    '资阳': '四川',
    '淄博': '山东',
    '自贡': '四川',
    '遵义': '贵州',
    '北京': '北京',
    '昌平': '北京',
    '天津': '天津',
    '蓟州': '天津',
    '滨海': '天津',
    '东丽': '天津',
    '上海': '上海',
    '重庆': '重庆',
    '涪陵': '重庆',
    '西永': '重庆',
    '云阳': '重庆',
}

province_china = (
    '河北',
    '山西',
    '辽宁',
    '吉林',
    '黑龙江',
    '江苏',
    '浙江',
    '安徽',
    '福建',
    '江西',
    '山东',
    '河南',
    '湖北',
    '湖南',
    '广东',
    '海南',
    '四川',
    '贵州',
    '云南',
    '陕西',
    '甘肃',
    '青海',
    '台湾',
    '内蒙古',
    '广西',
    '西藏',
    '宁夏',
    '新疆',
    '北京',
    '天津',
    '上海',
    '重庆',
    '香港',
    '澳门'
)