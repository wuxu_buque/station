Service=(
    '计算服务',
    '存储服务',
    '安全服务',
    '数据库服务',
    'IoT服务',
    'Iot服务',
    'EI服务',
    'PaaS服务',
    '媒体服务',
    '华为云咨询'
    '生态解决方案',
    '云商店',
)

ServiceCOA = {
    '计算服务': ('9226310', '9238470', '9241630', '9241490', '9241488', '6797562', '6797560'),
    '存储服务': ('9226340', '9240746', '9241546', '9241489', '9241502', '6798018', '6798016'),
    '安全服务': ('9226334', '9238410', '9241644', '9241498', '9241499', '6785982', '6785980'),
    '数据库服务': ('9226336', '9238466', '9241545', '9241500', '9241501', '6802226', '6802225'),
    'IoT服务': ('9226392', '9238168', '9241552', '9241349', '9241497', '6802224', '6802223'),
    'EI服务': ('9226342', '9241524', '9241558', '9241504', '9241523', '6798094', '6803867'),
    'PaaS服务': ('9226338', '9238412', '9241544', '9241505', '9241506', '6752772', '6752771'),
    'aPaaS服务': ('9226388', '9241526', '9241569', '9241537', '9241525', '6785142', '6803869'),
    '媒体服务': ('9226344', '9241511', '9241551', '9241509', '9241510', '6785140', '6803868'),
    '华为云咨询': ('9226386', '6785144', '6803870'),
    '生态解决方案': ('9240755', '6785148', '6803871'),
    '云商店': ('9226390', '9238468', '6801745', '6801744')
}

COAMap = {}

def initCOAMap():
    for key in ServiceCOA.keys():
        value = ServiceCOA[key]
        for v in value:
            COAMap[v] = key

initCOAMap()