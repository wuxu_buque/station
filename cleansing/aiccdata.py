import pandas as pd
from cleansing import Clean

'''
项目名称
Region名
RegionID
局点架构
开局版本
开局时间
交付状态
AZ数量
规模档位
模式
场景
'''

class AICCData(Clean):
    @classmethod
    def ReadData(cls, name: str, sheet_index=0):
        df = pd.read_excel(io=name, sheet_name=sheet_index)
        # df.drop(unused_cols, axis=1, inplace=True)

        # 更改列名
        # df.rename(columns={'所属基地': '基地名称',
        #                 '所属数据中心': '数据中心',
        #                 '省份': '区域', '云平台类型': '类型',
        #                 '云平台状态': '状态',
        #                 '当前版本': '版本'}, inplace=True)

        print(df)
        return df