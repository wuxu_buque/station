import pandas as pd
from cleansing import Clean

'''
手工账单数据清洗：
1、会计期
2、收入
3、产品编码
4、产品部
5、产品部COA
6、区域
'''
unused_cols = [
    '部门中文名称',
    '报表项1级中文名称',
    '报表项2级中文名称',
    '报表项3级中文名称',
    '报表项4级中文名称',
    '报表项5级中文名称',
    # '产业目录2级产品中文名称',
    '数据中心region云类型中文名称',
    '主产品Offering中文描述',
    '项目编码',
    # '项目中文名称',
    '重量级团队BMT中文名称',
    '地区部中文名称',
    '内外部',
    '区域责任中心代码',
    '损益国内海外',
    '国内或海外标志',
    '数据中心大区类别',
    '一级产品分类中文名称',
    '二级产品分类中文名称',
    '产业目录3级产品中文名称',
    '关联公司编码',
    '关联公司中文名称',
    '分录类别名称',
    '分录类别描述',
    '分录来源编码',
    '分录来源名称',
    '分录来源描述',
    '公司编码',
    '公司中文名称',
    '最终内部客户标识'
]

class GccData(Clean):
    @classmethod
    def ReadData(cls, name: str, sheet_index=0):
        df = pd.read_excel(io=name, sheet_name=sheet_index)

        # 去除'基地云收入分产品部拆分'收入
        df = df.loc[df['分录类别名称'].apply(lambda x: not x.startswith('基地云收入分产品部拆分'))]

        df.drop(unused_cols, axis=1, inplace=True)

        # 更改列名
        df.rename(columns={'经营金额_RMB实际汇率_总和': '收入',
                        '产业目录2级产品中文名称': '产品部',
                        'COA产品编码': '产品部COA',
                            '代表处中文名称': '区域'}, inplace=True)

        # 去除COA为1445、1512、1978的所有条目
        df['产品部COA'].apply(lambda x: cls.Str2Numeric(x))
        df = df.loc[df['产品部COA'].apply(lambda x: x != 9221978)]
        df = df.loc[df['产品部COA'].apply(lambda x: x != 9221445)]
        df = df.loc[df['产品部COA'].apply(lambda x: x != 9221512)]

        cls.CleanColumns(df.columns.values, df)

        # 去除项目列
        df.drop('项目中文名称', axis=1, inplace=True)
        print(df)
        return df