import pandas as pd
from pandas import DataFrame
from cleansing import cols_model
from cleansing import city_china
from cleansing import province_china
from cleansing import Service
from cleansing import COAMap

''' 
    '会计期',
    '收入',
    '基地',
    '区域',
    '产品编码',
    '产品名称',
    '产品部',
    '产品部COA'

手工拆分列: 会计期、基地、产品编码、产品名称、产品部、产品部COA、收入
GCC账单列: 会计期、收入、产品部、产品部COA、区域
'''

class Clean:
    @classmethod
    def NumericFilter(cls, x, num: int):
      if isinstance(x, str) and x.isdigit():
         return int(x) == num
      elif isinstance(x, int):
         return x == num
      else:
         return False

    @classmethod
    def NumericNotFilter(cls, x, num: int):
      if isinstance(x, str) and x.isdigit():
         return int(x) != num
      elif isinstance(x, int):
         return x != num
      else:
         return True

    @classmethod
    def Str2Numeric(cls, row):
        if row:
            if isinstance(row, str) and row.isdigit():
                row = int(row)
    @classmethod
    def CleanColumns(cls, cols, df: DataFrame):
        for col in cols_model:
            if col in cols:
                # 清洗数据
                cls.CleanColumn(col, df)
                continue
            # 新增列并新增数据
            cls.AddColumn(col, df)
    
    @classmethod
    def CleanProduct(cls, row):
        for service in Service:
            if row['产品部'].startswith(service):
                return service
        
        # 如果没有产品部，则通过COA查找
        if row['产品部COA'] in COAMap:
            return COAMap[row['产品部COA']]

        return ''

    @classmethod
    def CleanColumn(cls, col, df: DataFrame):
        if col == '会计期':
            df[col] = df[col].astype('str')
            # 去除202405~06数据
            df[col] = df[col].str[0:6]
            # 转换为时间格式
            df[col] = pd.to_datetime(df[col], format='%Y%m')
        if col == '区域':
            # 去除代表处
            df[col] = df[col].apply(lambda x: x.replace('代表处', ''))
        if col == '产品部':
            df[col] = df.apply(lambda row: cls.CleanProduct(row), axis=1)
    
    @staticmethod
    def Base2Region(row):
        for (i, city) in enumerate(city_china.keys()):
            if row['基地'].find(city) != -1:
                return city_china[city]
        for (i, province) in enumerate(province_china):
            if row['基地'].find(province) != -1:
                return province
        
        # 既不是省也不是市，则赋值为空
        return ''
    
    def Project2Base(row):
        for (i, city) in enumerate(city_china.keys()):
            if row['项目中文名称'].find(city) != -1:
                return city
        for (i, province) in enumerate(province_china):
            if row['项目中文名称'].find(province) != -1:
                return province
        
        # 既不是省也不是市，则赋值为空
        return ''

    @classmethod
    def AddColumn(cls, col: str, df: DataFrame):
        # gcc账单，通过项目反推基地
        if col == '基地':
            df[col] = df.apply(lambda row: cls.Project2Base(row), axis=1)
        if col in ('产品编码', '产品名称', '产品部COA'):
            df[col] = ''
        if col == '区域':
            df[col] = ''
            df[col] = df.apply(lambda row: cls.Base2Region(row), axis=1)