import pandas as pd
from cleansing import Clean

'''
手工账单数据清洗：
1、会计期
2、基地
3、产品编码
4、产品部
5、产品部COA
5、收入
'''
unused_cols = [
    '年度PO',
    '项目编码',
    '服务编码',
    '预算在公共',
    '项目总收入',
    '汇总',
    '备注1'
]

class HandData(Clean):
    @classmethod
    def HcsFilter(cls, x):
      if isinstance(x, str) and x.isdigit():
         return ((int(x) == 9221445) or (int(x) == 9221512))
      elif isinstance(x, int):
         return ((x == 9221445) or (x == 9221512))
      else:
         return False
      
    @classmethod
    def DateFilter(cls, x):
      if isinstance(x, str) and x.isdigit():
         return ((int(x) == 202301) or (int(x) == 202302) or (int(x) == 202303) or
                 (int(x) == 202304) or (int(x) == 202305))
      elif isinstance(x, int):
         return ((x == '202301') or (x == '202302') or (x == '202303') or
                 (x == '202304') or (x == '202305') or (x == '202306~07'))
      else:
         return False

    @classmethod
    def ReadHCSData(cls, name: str, sheet_index=0):
        df = pd.read_excel(io=name, sheet_name=sheet_index)
        # df['服务编码'].apply(lambda x: cls.Str2Numeric(x))
        # df['服务编码'].astype('str')
        # pd.to_numeric(df['服务编码'], errors='coerce').fillna('0')

        # 保留'服务编码'为9221445的行，这些内容是HCS且24年需要划给HCS的
        # df = df.loc[df['服务编码'].apply(lambda x: cls.HcsFilter(x))]
        df = df.loc[df['服务编码'].apply(lambda x: cls.NumericFilter(x, 9221512))]
        df = df.loc[df['会计期'].apply(lambda x: cls.DateFilter(x))]
        print(len(df))

        df = df.loc[df['产品部'].apply(lambda x: x.find('计算服务产品部')>=0)]
        print(len(df))

        # print(df)
        df.drop(unused_cols, axis=1, inplace=True)

        # 删除空行
        df.dropna(axis=0, how='all', inplace=True)

        # 更改列名
        df.rename(columns={'拆分收入': '收入'}, inplace=True)

        # cls.CleanColumns(df.columns.values, df)

        print(df)
        return df

    @classmethod
    def ReadData(cls, name: str, sheet_index=0):
        df = pd.read_excel(io=name, sheet_name=sheet_index)


        print(len(df))
        # df['产品部COA'].apply(lambda x: cls.Str2Numeric(x))
        # df['产品部COA'] = df['服务编码'].astype('int')
        # 去除'服务编码'为9221512的行，这些内容是HCS的行
        # df = df.loc[df['服务编码'].apply(lambda x: cls.NumericFilter(x, 9221445))]
        df = df.loc[df['服务编码'].apply(lambda x: cls.NumericNotFilter(x, 9221512))]

        # print(df)
        df.drop(unused_cols, axis=1, inplace=True)

        # 删除空行
        df.dropna(axis=0, how='all', inplace=True)

        # 更改列名
        df.rename(columns={'拆分收入': '收入'}, inplace=True)

        cls.CleanColumns(df.columns.values, df)

        print(df)
        return df

    #数据清洗




