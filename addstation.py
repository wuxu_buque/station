import pandas as pd
from cleansing import StationData
from decrypt import Decrypt
from model import Station

if __name__ == "__main__":
    # 读取基地数据
    df = StationData.ReadData('./data/平台信息.xlsx', 2)
    print(len(df))

    # 删除空行
    df.dropna(axis=0, how='all', inplace=True)

    # 1. 清洗站点信息
    res = Decrypt.StationInfo(df)
    print(res)
    Decrypt.StationInfoUpdate(res)