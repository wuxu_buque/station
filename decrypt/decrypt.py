import pandas as pd
from pandas import DataFrame
from db import DBContext
from model import ChinaIncome
from model import RegionIncome
from model import StationIncome
from model import Station
from model import AICC
import datetime


'''
1. 总收入
2. 区域基地云收入排行
3. 基地云收入排行
4. 基地收入分析
    1) 基地计算总收入
    2) 基地各服务收入占比排行
    3) 基地计算占比
    4) 基地计算收入折扣

'''
class Decrypt:
    @classmethod
    def ProductIncome(cls, df: DataFrame, res: dict):
        '''产品收入'''
        product = df['产品部'].values[0]
        total = df['收入'].sum()
        res[product] = total

    @classmethod
    def RegionIncome(cls, df, res: dict):
        '''区域总收入'''
        total = df['收入'].sum()
        region = df['区域'].values[0]
        # print('RegionIncome: ', region)

        # 分拆服务
        service = {}
        df.groupby('产品部').apply(lambda x: cls.ProductIncome(x, service))
        res[region] = (total, service)

    @classmethod
    def BaseIncome(cls, df, res: dict):
        '''基地总收入'''
        total = df['收入'].sum()
        base = df['基地'].values[0]
        region = df['区域'].values[0]
        # print('BaseIncome: ', base)

        # 分拆服务
        service = {}
        df.groupby('产品部').apply(lambda x: cls.ProductIncome(x, service))
        res[base] = (total, region, service)

    @classmethod
    def ChinaDetail(cls, df: DataFrame):
        '''总收入及各产品收入占比
        '''
        res = {}
        total = cls.Total(df)

        # 分拆服务
        df.groupby('产品部').apply(lambda x: cls.ProductIncome(x, res))

        res['其他'] = total - sum(res.values())
        return (total, res)
    
    @classmethod
    def ChinaDetailUpdate(cls, total: float, res: dict):
        # 1. 清理历史数据
        with DBContext('w', None, True) as session:
            session.query(ChinaIncome).filter(ChinaIncome.when == datetime.date(2023, 1, 1)).delete(synchronize_session=False)

        # 2. 更新历史数据
        computeincome = res['计算服务'] if '计算服务' in res else 0
        storageincome = res['存储服务'] if '存储服务' in res else 0
        secureincome = res['安全服务'] if '安全服务' in res else 0
        dbincome = res['数据库服务'] if '数据库服务' in res else 0
        iotincome = res['IoT服务']  if 'IoT服务' in res else 0
        eiincome = res['EI服务'] if 'EI服务' in res else 0 
        paasincome = res['PaaS服务'] if 'PaaS服务' in res else 0 
        apaasincome = res['aPaaS服务'] if 'aPaaS服务' in res else 0 
        mediaincome = res['媒体服务'] if '媒体服务' in res else 0 
        consultincome = res['华为云咨询'] if '华为云咨询' in res else 0 
        commonincome = 0
        shopincome = res['云商店'] if '云商店' in res else 0 
        solutionincome = res['解决方案'] if '解决方案' in res else 0
        otherincome = res['其他'] if '其他' in res else 0 
        when = datetime.datetime.strptime('2023', '%Y')
        china = ChinaIncome(name='中国区', total=total, 
                            computeincome=computeincome, 
                            storageincome=storageincome, 
                            secureincome=secureincome, 
                            dbincome=dbincome, 
                            iotincome=iotincome, 
                            eiincome=eiincome, 
                            paasincome=paasincome,
                            apaasincome=apaasincome,
                            mediaincome=mediaincome, 
                            consultincome=consultincome, 
                            commonincome=commonincome,
                            shopincome=shopincome, 
                            solutionincome=solutionincome, 
                            otherincome=otherincome, 
                            when=when, 
                            content='NA')
        with DBContext('w', None, True) as session:
            session.add(china)
        

    @classmethod
    def RegionDetail(cls, df: DataFrame):
        '''区域总收入及各产品收入占比
        '''
        res = {}

        # 分拆区域
        df.groupby('区域').apply(lambda x: cls.RegionIncome(x, res))
        return res
    
    @classmethod
    def RegionDetailUpdate(cls, res: dict):
        # 1. 清理历史数据
        with DBContext('w', None, True) as session:
            session.query(RegionIncome).filter(RegionIncome.when == datetime.date(2023, 1, 1)).delete(synchronize_session=False)

        # 2. 更新历史数据
        for region in res:
            total = res[region][0]
            service = res[region][1]
            computeincome = service['计算服务'] if '计算服务' in service else 0
            storageincome = service['存储服务'] if '存储服务' in service else 0
            secureincome = service['安全服务'] if '安全服务' in service else 0
            dbincome = service['数据库服务'] if '数据库服务' in service else 0
            iotincome = service['IoT服务']  if 'IoT服务' in service else 0
            eiincome = service['EI服务'] if 'EI服务' in service else 0 
            paasincome = service['PaaS服务'] if 'PaaS服务' in service else 0 
            apaasincome = service['aPaaS服务'] if 'aPaaS服务' in service else 0 
            mediaincome = service['媒体服务'] if '媒体服务' in service else 0 
            consultincome = service['华为云咨询'] if '华为云咨询' in service else 0 
            commonincome = 0
            shopincome = service['云商店'] if '云商店' in service else 0 
            solutionincome = service['解决方案'] if '解决方案' in service else 0
            otherincome = service['其他'] if '其他' in service else 0 
            when = datetime.datetime.strptime('2023', '%Y')
            region = RegionIncome(region=region,
                                total=total,
                                computeincome=computeincome, 
                                storageincome=storageincome, 
                                secureincome=secureincome, 
                                dbincome=dbincome, 
                                iotincome=iotincome, 
                                eiincome=eiincome, 
                                paasincome=paasincome,
                                apaasincome=apaasincome,
                                mediaincome=mediaincome, 
                                consultincome=consultincome, 
                                commonincome=commonincome,
                                shopincome=shopincome, 
                                solutionincome=solutionincome, 
                                otherincome=otherincome, 
                                when=when, 
                                content='NA')
            with DBContext('w', None, True) as session:
                session.add(region)

    @classmethod
    def StationDetail(cls, df: DataFrame):
        '''基地总收入及各产品收入占比
        '''
        res = {}

        # 分拆区域
        df.groupby('基地').apply(lambda x: cls.BaseIncome(x, res))

        # 基地为空的其他收入
        service = {}
        ds = df.loc[df['基地'].isna()]
        df.groupby('产品部').apply(lambda x: cls.ProductIncome(x, service))
        res['其他'] = (cls.Total(ds), df['区域'].values[0], service)
        return res

    @classmethod
    def StationDetailUpdate(cls, res: dict):
        # 1. 清理历史数据
        with DBContext('w', None, True) as session:
            session.query(StationIncome).filter(StationIncome.when == datetime.date(2023, 1, 1)).delete(synchronize_session=False)

        # 2. 更新历史数据
        for name in res:
            total = res[name][0]
            region = res[name][1]
            service = res[name][2]
            computeincome = service['计算服务'] if '计算服务' in service else 0
            storageincome = service['存储服务'] if '存储服务' in service else 0
            secureincome = service['安全服务'] if '安全服务' in service else 0
            dbincome = service['数据库服务'] if '数据库服务' in service else 0
            iotincome = service['IoT服务']  if 'IoT服务' in service else 0
            eiincome = service['EI服务'] if 'EI服务' in service else 0 
            paasincome = service['PaaS服务'] if 'PaaS服务' in service else 0 
            apaasincome = service['aPaaS服务'] if 'aPaaS服务' in service else 0 
            mediaincome = service['媒体服务'] if '媒体服务' in service else 0 
            consultincome = service['华为云咨询'] if '华为云咨询' in service else 0 
            commonincome = 0
            shopincome = service['云商店'] if '云商店' in service else 0 
            solutionincome = service['解决方案'] if '解决方案' in service else 0
            otherincome = service['其他'] if '其他' in service else 0 
            when = datetime.datetime.strptime('2023', '%Y')
            region = StationIncome(name=name,
                                region=region,
                                total=total,
                                computeincome=computeincome, 
                                storageincome=storageincome, 
                                secureincome=secureincome, 
                                dbincome=dbincome, 
                                iotincome=iotincome, 
                                eiincome=eiincome, 
                                paasincome=paasincome,
                                apaasincome=apaasincome,
                                mediaincome=mediaincome, 
                                consultincome=consultincome, 
                                commonincome=commonincome,
                                shopincome=shopincome, 
                                solutionincome=solutionincome, 
                                otherincome=otherincome, 
                                when=when, 
                                content='NA')
            with DBContext('w', None, True) as session:
                session.add(region)

    @classmethod
    def StationInfo(cls, df: DataFrame):
        '''基地基本信息
        '''

        df['基地经理姓名'] = df['基地经理姓名'].fillna('NA')
        df['基地经理工号'] = df['基地经理工号'].fillna('NA')
        res = []
        # 基地为空的其他收入
        for index, row in df.iterrows():
            name = row['基地名称']
            region = row['区域']
            datacenter = row['数据中心']
            mode = row['商业模式']
            type = row['类型']
            version = row['版本']
            state = row['状态']
            director = row['基地经理姓名']
            jobnumber = row['基地经理工号']
            when = datetime.datetime.strptime(row['更新时间'], '%Y-%m-%d %H:%M:%S')
            context = 'NA'
            station = Station(name, region, datacenter, mode, type, version, state, director, jobnumber, when, context)
            res.append(station)
        return res

    @classmethod
    def StationInfoUpdate(cls, res: list):
        # 1. 清理历史数据
        with DBContext('w', None, True) as session:
            session.query(Station).delete(synchronize_session=False)

        # 2. 更新历史数据
        for station in res:
          with DBContext('w', None, True) as session:
            session.add(station)

    @classmethod
    def AICCInfo(cls, df: DataFrame):
        '''AICC基本信息
        '''

        # df['基地经理姓名'] = df['基地经理姓名'].fillna('NA')
        # df['基地经理工号'] = df['基地经理工号'].fillna('NA')
        res = []
        # 基地为空的其他收入
        for index, row in df.iterrows():
            name = row['项目名称']
            region = row['Region名']
            regionid = row['RegionID']
            arch = row['局点架构']
            version = row['开局版本']
            when = datetime.datetime.strptime(row['开局时间'], '%Y-%m-%d')
            state = row['交付状态']
            azcount = int(row['AZ数量'])
            scale = int(row['规模档位'])
            mode = row['模式']
            scene = row['场景']
            aicc = AICC(name=name, region=region, regionid=regionid, arch=arch, 
                        version=version, when=when, state=state, azcount=azcount,
                        scale=scale, mode=mode, scene=scene)
            res.append(aicc)
        return res

    @classmethod
    def AICCInfoUpdate(cls, res: list):
        # 1. 清理历史数据
        with DBContext('w', None, True) as session:
            session.query(AICC).delete(synchronize_session=False)

        # 2. 更新历史数据
        for aicc in res:
          with DBContext('w', None, True) as session:
            session.add(aicc)

    # 总收入
    @classmethod
    def Total(cls, df: DataFrame):
        total = df['收入'].sum()
        print('总收入:', total)
        return total

    # 统计区域收入
    @classmethod
    def process_group(cls, df, res: dict, col):
        region = df.iloc[0, col]
        total = df['收入'].sum()
        res[region] = total
        print(region, ":", total)

    # 区域基地云收入排行    
    @classmethod
    def RegionRanking(cls, df: DataFrame):
        res = {}
        df.groupby('区域').apply(lambda x: cls.process_group(x, res, 7))
        print(sorted(res.items(), key=lambda x:(-x[1], x[0])))

    # 基地云收入排行
    @classmethod
    def BaseRanking(cls, df: DataFrame):
        res = {}
        df.groupby('基地').apply(lambda x: cls.process_group(x, res, 1))
        print(sorted(res.items(), key=lambda x:(-x[1], x[0])))
        return res

    # 基地收入分析
    # @classmethod
    # def BaseDetail(cls, df: DataFrame):
    #     cls.BaseTotal(df)
    #     cls.BaseServiceRatio(df)
    #     cls.BaseComputeRatio(df)
    #     cls.BaseComputeDiscount(df)

    # 1) 基地计算总收入
    @classmethod
    def BaseTotal(cls, df: DataFrame):
        res = {}
        # 剔除计算外的item
        df = df.loc[df['产品部'].apply(lambda x: x=='计算服务')]
        df.groupby('基地').apply(lambda x: cls.process_group(x, res, 1))
        print(sorted(res.items(), key=lambda x:(-x[1], x[0])))
        return res

    # 统计产品收入
    @classmethod
    def product_group(cls, df, tatal: dict, base: dict, col):
        name = df.iloc[0, col]
        total = df['收入'].sum()
        total[name] = total
        print(name, ":", total)

        # 基地各服务收入占比
        df.groupby('产品部').apply(lambda x: cls.process_group(x, base, 1))


    # 2) 基地各服务收入占比排行
    @classmethod
    def BaseServiceRatio(cls, df: DataFrame):
        total_res = {}
        base_res = {}

        # 筛选为空的基地
        ds = df.loc[df['基地'].isna()]

        # 各基地总收入及各服务分拆
        ds.groupby('基地').apply(lambda x: cls.product_group(x, total_res, base_res, 1))


    # 3) 基地计算占比
    @classmethod
    def BaseComputeRatio(cls, df: DataFrame):
        pass

    # 4) 基地计算收入折扣
    @classmethod
    def BaseComputeDiscount(cls, df: DataFrame):
        pass

    # 5) 区域的基地云分析
    # @classmethod
    # def RegionDetail(cls, region: str, df: DataFrame):
    #     df = df.loc[df['区域'].apply(lambda x: x==region)]
    #     total = cls.BaseRanking(df)
    #     compute = cls.BaseTotal(df)
    #     res = {}
        
    #     # 交集
    #     a = total.keys() & compute.keys()

    #     for base in total.keys():
    #         if base in a:
    #             item = [total[base], compute[base], compute[base]/total[base]]
    #             res[base] = item

    #     print('#####%s#####】' %region)
    #     print("%-10s  %-12s  %-10s  %-8s" %('基地', '总收入', '计算收入', '计算收入占比'))
    #     for base in res.keys():
    #         print('%-10s  %-12.0f  %-12.0f  %.2f' %(base, res[base][0], res[base][1], res[base][2]))
    #     print('###########')

