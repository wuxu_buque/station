import pandas as pd
from cleansing import AICCData
from decrypt import Decrypt
from model import AICC

if __name__ == "__main__":
    # 读取基地数据
    df = AICCData.ReadData('./data/AICC20231222.xlsx', 0)
    print(len(df))

    # 删除空行
    df.dropna(axis=0, how='all', inplace=True)

    # 1. 清洗站点信息
    res = Decrypt.AICCInfo(df)
    print(res)
    Decrypt.AICCInfoUpdate(res)