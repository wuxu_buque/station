from cleansing import HandData
from pandas import DataFrame
import pandas as pd
from cleansing import ClassifyResourceProduct



def StationIncome(df: DataFrame, result: dict):
    '''基地收入'''
    station = df.name
    total = df['收入'].sum()

    com, hosting = ClassifyResourceProduct()
    hosting_total = 0
    com_total = 0
    for index, row in df.iterrows():
        if row['产品名称'] in hosting:
            hosting_total += row['收入']
        else:
            com_total += row['收入']

    result[station] = (total, com_total, hosting_total)

if __name__ == "__main__":
  
    res = {}
    # 读取人工数据
    hand = HandData.ReadHCSData('./data/1~12月基地云手工账单拆分明细0109.xlsx', 4)
    print(len(hand))

    hand.groupby('基地').apply(lambda x: StationIncome(x, res))
    hosting_total = 0
    com_total = 0
    total = 0
    l = sorted(res.items(), key = lambda x:(x[1][1], x[0][1]), reverse=True)
    # print(l)
    for item in l:
        # print('%s: %.f %.f %.f' %(item[0], item[1][0], item[1][1], item[1][2]))
        print('%-10s: %.f' %(item[0], item[1][1]))
        hosting_total += item[1][1]
        com_total += item[1][2]
        total += item[1][0]
    print(total, com_total, hosting_total)



    # df.to_excel('./data/out.xlsx', sheet_name='汇总', index=False)
    # # print(df)

    # # 读取已分析的数据
    # df = pd.read_excel('./data/out.xlsx', sheet_name=0)

    # # 1. 中国区
    # (total, res) = Decrypt.ChinaDetail(df)
    # print(res)
    # Decrypt.ChinaDetailUpdate(total, res)

    # # 2. 区域代表处
    # res = Decrypt.RegionDetail(df)
    # print(res)
    # Decrypt.RegionDetailUpdate(res)

    # # 3. 基地
    # res = Decrypt.StationDetail(df)
    # print(res)
    # res = Decrypt.StationDetailUpdate(res)
