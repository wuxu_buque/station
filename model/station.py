import datetime
from sqlalchemy import Column, String, Integer, DateTime, Float
from sqlalchemy.ext.declarative import declarative_base
from db import DBContext

Base = declarative_base()

# 基地信息
class Station(Base):
    __tablename__ = 'Station'
    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True, comment="id")
    name = Column(String(32), nullable=False, comment="基地名称")
    region = Column(String(32), nullable=False, comment="基地区域")
    datacenter = Column(String(128), nullable=False, comment="数据中心")
    mode = Column(String(128), nullable=False, comment="商业模式")
    type = Column(String(32), nullable=False, comment="类型")
    version = Column(String(128), nullable=False, comment="版本")
    state = Column(String(32), nullable=False, comment="项目状态")
    director = Column(String(32), nullable=False, comment="经理")
    jobnumber = Column(String(32), nullable=False, comment="工号")
    when = Column(DateTime, nullable=False, comment="收入时间")
    content = Column(String(512), nullable=False, comment="备注")

    def __init__(self, name: str, region: str, datacenter: str, mode: str,
                type: str, version: str, state: str, director: str, 
                jobnumber: str, when: datetime, content: str) -> None:
        self.name = name
        self.region = region
        self.datacenter = datacenter
        self.mode = mode
        self.type = type
        self.version = version
        self.state = state
        self.director = director
        self.jobnumber = jobnumber
        self.when = when
        self.content = content
        super().__init__()

Base.metadata.create_all(bind=DBContext.get_db_engine())

__all__=[
    "Station",
]