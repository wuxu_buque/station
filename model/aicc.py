import datetime
from sqlalchemy import Column, String, Integer, DateTime, Float
from sqlalchemy.ext.declarative import declarative_base
from db import DBContext

Base = declarative_base()

# AICC信息
class AICC(Base):
    __tablename__ = 'AICC'
    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True, comment="id")
    name = Column(String(32), nullable=False, comment="项目名称")
    region = Column(String(32), nullable=False, comment="region名")
    regionid = Column(String(128), nullable=False, comment="regionid")
    arch = Column(String(128), nullable=False, comment="局点架构")
    version = Column(String(128), nullable=False, comment="开局版本")
    when = Column(DateTime, nullable=False, comment="开局时间")
    state = Column(String(32), nullable=False, comment="交付状态")
    azcount = Column(Integer, nullable=False, comment="az数量")
    scale = Column(Integer, nullable=False, comment="规模档位")
    mode = Column(String(32), nullable=False, comment="模式")
    scene = Column(String(32), nullable=False, comment="场景")

    def __init__(self, name: str, region: str, regionid: str, arch: str,
                version: str, when: datetime, state: str, azcount: Integer, 
                scale: Integer, mode: str, scene: str) -> None:
        self.name = name
        self.region = region
        self.regionid = regionid
        self.arch = arch
        self.version = version
        self.when = when
        self.state = state
        self.azcount = azcount
        self.scale = scale
        self.mode = mode
        self.scene = scene
        super().__init__()

Base.metadata.create_all(bind=DBContext.get_db_engine())

__all__=[
    "AICC",
]