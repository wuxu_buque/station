import datetime
from sqlalchemy import Column, String, Integer, DateTime, Float
from sqlalchemy.ext.declarative import declarative_base
from db import DBContext

Base = declarative_base()

# 中国区收入明细
class ChinaIncome(Base):
    __tablename__ = 'ChinaIncome'
    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True, comment="id")
    name = Column(String(32), primary_key=False, comment="中国区")
    total = Column(Float, nullable=False, comment="总收入")
    computeincome = Column(Float, nullable=False, comment="计算云服务收入")
    storageincome = Column(Float, nullable=False, comment="存储云服务收入")
    secureincome = Column(Float, nullable=False, comment="安全云服务收入")
    dbincome = Column(Float, nullable=False, comment="数据库云服务收入")
    iotincome = Column(Float, nullable=False, comment="iot云服务收入")
    eiincome = Column(Float, nullable=False, comment="ei云服务收入")
    paasincome = Column(Float, nullable=False, comment="paas云服务收入")
    apaasincome = Column(Float, nullable=False, comment="apaas云服务收入")
    mediaincome = Column(Float, nullable=False, comment="媒体云服务收入")
    consultincome = Column(Float, nullable=False, comment="咨询云服务收入")
    commonincome = Column(Float, nullable=False, comment="公共云服务收入")
    shopincome = Column(Float, nullable=False, comment="云商店云服务收入")
    solutionincome = Column(Float, nullable=False, comment="解决方案云服务收入")
    when = Column(DateTime, nullable=False, comment="收入时间")
    content = Column(String(256), nullable=False, comment="备注")
    otherincome = Column(Float, nullable=False, comment="其他收入")
    def __init__(self, name: str, total: Float, computeincome: Float, storageincome: Float, 
                 secureincome: Float, dbincome: Float, iotincome: Float, eiincome: Float,
                 paasincome: Float, apaasincome: Float, mediaincome: Float, 
                 consultincome: Float, commonincome: Float, shopincome: Float,
                 solutionincome: Float, otherincome: Float, when: datetime, content: str) -> None:
        self.name = name
        self.total = total
        self.computeincome = computeincome
        self.storageincome = storageincome
        self.secureincome = secureincome
        self.commonincome = commonincome
        self.dbincome = dbincome
        self.iotincome = iotincome
        self.eiincome = eiincome
        self.paasincome = paasincome
        self.apaasincome = apaasincome
        self.mediaincome = mediaincome
        self.consultincome = consultincome
        self.commonincome = commonincome
        self.shopincome = shopincome
        self.solutionincome = solutionincome
        self.otherincome = otherincome
        self.when = when
        self.content = content
        super().__init__()

Base.metadata.create_all(bind=DBContext.get_db_engine())

__all__=[
    "ChinaIncome",
]