import datetime
from sqlalchemy import Column, String, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from db import DBContext

Base = declarative_base()

# 系统日志
class SystemLog(Base):
    __tablename__ = 'SystemLog'
    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True, comment="id")
    content = Column(String(256), default=None, nullable=False, comment="日志内容")
    module = Column(String(32), default=None, nullable=False, comment="日志模块")
    user = Column(String(32), default=None, nullable=False, comment="操作人员")
    createtime = Column(DateTime, default=None, nullable=False, comment="日志时间")

    def __init__(self, content: str, module: str, user: str, createtime: str=None) -> None:
        self.content = content
        self.user = user
        self.module = module
        if self.createtime:
          self.createtime = datetime.datetime.strptime(createtime, '%Y-%m-%d %H:%M:%S')
        else:
          self.createtime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        super().__init__()
    
    def SysLog(self, content: str, user: str, module: str):
        log = SystemLog(content, user, module, datetime.datetime.now())
        with DBContext('w', None, True) as session:
            session.add(log)

    def add(self):
        with DBContext('w', None, True) as session:
            session.add(self)

Base.metadata.create_all(bind=DBContext.get_db_engine())

__all__=[
    "SystemLog",
]