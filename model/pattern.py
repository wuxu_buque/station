from sqlalchemy import Column, String, Integer, Float
from sqlalchemy.ext.declarative import declarative_base
from db import DBContext


Base = declarative_base()

# 格局信息
class Pattern(Base):
    __tablename__ = 'Pattern'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True, comment="id")
    province = Column(String(32), primary_key=False, comment="省份")
    city = Column(String(32), primary_key=False, comment="地市")
    space = Column(Float(4), nullable=False, comment="总空间")
    content = Column(String(256), nullable=True, comment="备注")

    def __init__(self, province: str, city: str, space: float, content: str) -> None:
        self.province = province
        self.city = city
        self.space = space
        self.content = content
        # if self.createtime:
        #   self.createtime = datetime.datetime.strptime(createtime, '%Y-%m-%d %H:%M:%S')
        # else:
        #   self.createtime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        super().__init__()

Base.metadata.create_all(bind=DBContext.get_db_engine())

__all__=[
    "Pattern",
]