import time
from sqlalchemy import Column, String, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from db import DBContext

Base = declarative_base()

# 用户信息
class UserInfo(Base):
    __tablename__ = 'User'
    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True, comment="id")
    name = Column(String(32), nullable=False, comment="用户名")
    displayName = Column(String(32), nullable=False, comment="显示用户名")
    phone = Column(String(14), default=None, nullable=False, comment="手机号码")
    password = Column(String(256), default=None, nullable=False, comment="密码")
    mail = Column(String(32), default=None, nullable=False, comment="邮箱")
    role = Column(String(32), default=None, nullable=False, comment="角色")
    createtime = Column(DateTime, nullable=False, comment="创建时间")

    def __init__(self, name: str, displayName: str, phone: str, password: str, mail: str, role: str) -> None:
        self.name = name
        self.displayName = displayName
        self.phone = phone
        self.password = password
        self.mail = mail
        self.role = role
        self.isadmin = False
        if role == 'admin':
            self.isadmin = True
        self.createtime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
        super().__init__()


Base.metadata.create_all(bind=DBContext.get_db_engine())

__all__=[
    "UserInfo",
]
