import datetime
from sqlalchemy import Column, String, Integer, DateTime, Float
from sqlalchemy.ext.declarative import declarative_base
from db import DBContext

Base = declarative_base()

# 项目信息
class Project(Base):
    __tablename__ = 'Project'
    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True, comment="id")
    province = Column(String(32), primary_key=False, comment="省份")
    location = Column(String(32), primary_key=False, comment="基地")
    name = Column(String(128), primary_key=True, comment="项目名称")
    budget = Column(Float(4), nullable=False, comment="预算")
    vendor = Column(String(128), nullable=False, comment="厂商")
    tendertime = Column(DateTime, nullable=False, comment="招标时间")
    begintime = Column(DateTime, nullable=False, comment="交付时间")
    endtime = Column(DateTime, nullable=False, comment="结束时间")
    state = Column(Integer, nullable=False, comment="项目状态")
    content = Column(String(1024), nullable=False, comment="备注")

    def __init__(self, province: str, city: str, name: str, budget: float,
                vender: str, tendertime: datetime, begintime: datetime, 
                endtime: datetime, state: int, content: str) -> None:
        self.province = province
        self.city = city
        self.name = name
        self.budget = budget
        self.vendor = vender
        self.tendertime = tendertime
        self.begintime = begintime
        self.endtime = endtime
        self.state = state
        self.content = content
        super().__init__()

Base.metadata.create_all(bind=DBContext.get_db_engine())

__all__=[
    "Project",
]